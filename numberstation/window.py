# window.py
#
# Copyright Martijn Braam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk, GLib, GObject, Gio, Gdk, GLib

from base64 import b32decode
from datetime import datetime
from collections import namedtuple

import keyring
import json

import gi
from keyring.errors import KeyringLocked

from numberstation.otpurl import OTPUrl

@Gtk.Template(resource_path='/org/postmarketos/numberstation/ui/window.ui')
class NumberstationWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'NumberstationWindow'

    #On main navigation view
    codes = Gtk.Template.Child()
    listbox = Gtk.Template.Child()
    window_status_page = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    main_stack = Gtk.Template.Child()
    add_entry_button = Gtk.Template.Child()
    back_button = Gtk.Template.Child()

    #On add entry navigation page
    add_entry_page_button = Gtk.Template.Child()
    name_buffer = Gtk.Template.Child()
    type_otp = Gtk.Template.Child()
    secret_key_buffer = Gtk.Template.Child()
    add_timer = Gtk.Template.Child()
    add_counter = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.application = Gio.Application.get_default()

        self.set_icon_name("org.postmarketos.Numberstation")

        self.init_actions()
        self.tokens = []
        self.gestures = []
        Gtk.Widget.add_css_class (self.listbox, "navigation-sidebar")

        try:
            all_keys = keyring.get_password('numberstation', 'totp')
            if all_keys is not None:
                for url in json.loads(all_keys):
                    try:
                        temp = OTPUrl(url)
                        temp.get_token()
                        self.tokens.append(temp)
                    except:
                        print("Invalid key: {}, skipping".format(url))
        except KeyringLocked:
            self.show_error("The keyring could not be opened")
            print("Could not unlock the keyring")

        self.timers = []

        if len(self.tokens) >= 1:
            self.build_code_list()

        GLib.timeout_add(1000, self.update_codes)

    def init_actions(self):
        action = Gio.SimpleAction.new("import", None)
        action.connect("activate", self.on_import)
        self.application.add_action(action)

        action = Gio.SimpleAction.new("export", None)
        action.connect("activate", self.on_export)
        self.application.add_action(action)

    def save_keyring(self):
        print("Saving keyring")
        key_urls = []
        for token in self.tokens:
            key_urls.append(token.get_url())
        password = json.dumps(key_urls)
        keyring.set_password('numberstation', 'totp', password)

    def on_hotp_click(self, widget, *args):
        clipboard = Gdk.Display.get_clipboard(Gdk.Display.get_default())
        #TODO This doesn't work
        #clipboard.set_text(widget.get_label().replace(' ', ''))
        widget.token.initial_count += 1
        self.save_keyring()
        self.update_code_label(widget, None, widget.token)

    def on_context_menu(self, eb):
        popup = Gtk.Popover()

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        box.set_spacing(6)
        box.set_margin_top(12)
        box.set_margin_bottom(12)
        box.set_margin_start(12)
        box.set_margin_end(12)
        label = Gtk.Entry()
        label.set_text(eb.token.label)
        label.index = eb.index
        label.connect("activate", self.on_update_label)
        box.append(label)

        btn_del = Gtk.Button()
        btn_del.set_icon_name("user-trash-symbolic")
        btn_del.get_style_context().add_class("destructive-action")
        btn_del.index = eb.index
        btn_del.connect('clicked', self.on_delete)
        box.append(btn_del)

        popup.set_parent(eb)
        popup.set_child(box)
        popup.popup()

    #TODO Need to test
    def on_long_press(self, gesture, x, y, *args):
        eb = gesture.eb
        self.on_context_menu(eb)

    def on_right_click(self, ctrl, n_press, x, y):
        eb = ctrl.eb
        self.on_context_menu(eb)

    def on_delete(self, widget, *args):
        index = widget.index
        del self.tokens[index]

        self.save_keyring()
        self.build_code_list()
        toast = Adw.Toast.new ("Deleted Code")
        Adw.ToastOverlay.add_toast(self.toast_overlay, toast)

    def on_update_label(self, widget, *args):
        new_label = widget.get_text()
        index = widget.index
        self.tokens[index].label = new_label
        self.save_keyring()
        self.build_code_list()
        toast = Adw.Toast.new ("Changed Title")
        Adw.ToastOverlay.add_toast(self.toast_overlay, toast)

    def update_codes(self):
        GLib.timeout_add(1000, self.update_codes)
        for timer in self.timers:
            if timer.valid_until is None:
                continue
            if timer.valid_until < datetime.now():
                self.update_code_label(timer.display, timer, timer.token)
            timer.set_fraction(timer.token.get_validity())

    def update_code_label(self, label, progressbar, token):
        code, valid_until = token.get_token()
        if code is None:
            return
        user_code = code
        paste_code = code
        if len(code) % 4 == 0:
            user_code = ' '.join([code[i:i + 4] for i in range(0, len(code), 4)])
        elif len(code) % 3 == 0:
            user_code = ' '.join([code[i:i + 3] for i in range(0, len(code), 3)])
        elif len(code) % 2 == 0:
            user_code = ' '.join([code[i:i + 2] for i in range(0, len(code), 2)])
        if token.type == 'totp':
            label.set_text(user_code)
        else:
            label.set_label(user_code)
        label.paste_code = paste_code
        if valid_until:
            progressbar.valid_until = valid_until
        else:
            if progressbar:
                progressbar.valid_until = None

    def build_code_list(self):
        # This doesn't show the placeholder for some reason
        # if there are no tokens
        self.listbox.remove_all()

        for index, token in enumerate(self.tokens):
            eb = Adw.Bin()
            grid = Gtk.Grid()

            grid.set_hexpand(True)
            display_label = token.label
            if ':' in token.label:
                part = token.label.split(':', maxsplit=1)
                display_label = f"{part[0]} ({part[1]})"
            label = Gtk.Label()
            label.set_wrap(True)
            label.set_label(display_label)
            #TODO This doesn't seem to do anything
            Gtk.Widget.add_css_class(label, "token-label")
            grid.attach(label, 0, 0, 1, 1)

            if token.type == 'totp':
                code = Gtk.Label()
                code.set_label ("000000")
            elif token.type == 'hotp':
                code = Gtk.Button()
                code.set_label ("000000")
                code.set_hexpand(True)
                code.connect('clicked', self.on_hotp_click)

            #TODO This doesn't seem to do anything
            Gtk.Widget.add_css_class(code, "token-code")
            code.token = token
            grid.attach(code, 0, 1, 1, 1)
            timer = Gtk.ProgressBar()
            timer.set_hexpand(True)
            timer.period = token.period

            self.update_code_label(code, timer, token)
            timer.token = token
            timer.display = code
            if not hasattr(timer, 'valid_until'):
                continue
            if timer.valid_until is not None:
                timer.show()
                timer.set_fraction(token.get_validity())
            else:
                timer.hide()
            self.timers.append(timer)
            grid.attach(timer, 0, 2, 1, 1)
            eb.set_child(grid)
            eb.token = token
            eb.index = index
            self.num_tokens = index

            right_click = Gtk.GestureClick.new()
            right_click.set_button(3)
            eb.add_controller(right_click)
            right_click.eb = eb
            right_click.connect('released', self.on_right_click)
            longpress = Gtk.GestureLongPress.new()
            self.add_controller(longpress)
            longpress.eb = eb
            #TODO long press activates on pressing button, need to fix
            #longpress.connect('pressed', self.on_long_press)
            #TODO It would be nice to auto copy here on a click
            self.listbox.prepend(eb)

    @Gtk.Template.Callback()
    def on_add_entry_button_clicked(self, widget, *args):
        self.main_stack.push_by_tag("page-2")
        self.add_entry_button.hide()
        self.back_button.show()

    def reset_add_entry_page(self):
          self.name_buffer.set_text("", 0)
          self.type_otp.set_selected(0)
          self.secret_key_buffer.set_text("", 0)
          self.add_timer.set_value(30)
          self.add_counter.set_value(6)
          self.main_stack.pop_to_tag("page-1")
          self.add_entry_button.show()
          self.back_button.hide()

    @Gtk.Template.Callback()
    def on_back_button_clicked(self, widget, *args):
        self.reset_add_entry_page()

    @Gtk.Template.Callback()
    def add_entry_add_button_clicked(self, widget, *args):
      name = self.name_buffer.get_text().strip()
      selected = self.type_otp.get_selected()
      type = 'totp' if selected == 0 else 'hotp'
      secret = self.secret_key_buffer.get_text().strip()
      duration = self.add_timer.get_value_as_int()
      counter = self.add_counter.get_value_as_int()

      if not name:
          toast = Adw.Toast.new ("Name is Empty")
          Adw.ToastOverlay.add_toast(self.toast_overlay, toast)
          return

      if not secret:
          toast = Adw.Toast.new ("Secret is Empty")
          Adw.ToastOverlay.add_toast(self.toast_overlay, toast)
          return

      existing = False
      url = OTPUrl.create(name=name, type=type, secret=secret, duration=duration, length=length, counter=counter)
      for token in self.tokens:
          if token.secret == url.secret:
              self.show_error("The code you tried to add already exists in the database")
              print("The code you tried to add already exists in the database, skipping...")

              toast = Adw.Toast.new ("Code Already Exists")
              Adw.ToastOverlay.add_toast(self.toast_overlay, toast)

              existing = True

      if not existing:
          self.tokens.append(url)
          self.save_keyring()

          self.build_code_list()
          self.reset_add_entry_page()

    def show_error(self, message):
        self.error.set_text(message)
        self.error.show()

    def import_url(self, url):
        try:
            url = OTPUrl(url)
        except ValueError as e:
            self.show_error(str(e))
            print(e)
            return

        existing = False
        for token in self.tokens:
            if token.secret == url.secret:
                self.show_error("The code you tried to add already exists in the database")
                print("The code you tried to add already exists in the database, skipping...")
                existing = True

        if not existing:
            self.tokens.append(url)
            self.save_keyring()

    def import_dialog_open_callback(self, dialog, result):
        try:
            file = dialog.open_finish(result)
            if file is not None:
                print(f"File path is {file.get_path()}")
                filename = file.get_path()
                with open(filename, 'r') as handle:
                    for line in handle.readlines():
                        if '://' in line:
                            self.import_url(line.strip())
                    self.build_code_list()
        except GLib.Error as error:
            print(f"Error opening file: {error.message}")

    def on_import(self, *args):
        self.open_dialog = Gtk.FileDialog.new()
        self.open_dialog.set_title("Import a File")
        self.open_dialog.open(self, None, self.import_dialog_open_callback)

    def export_dialog_open_callback(self, dialog, result):
        try:
            file = dialog.save_finish(result)
            if file is not None:
                print(f"File path is {file.get_path()}")
                filename = file.get_path()
                raw = []
                for token in self.tokens:
                    raw.append(token.get_url() + "\n")
                with open(filename, 'w') as handle:
                    handle.writelines(raw)
        except GLib.Error as error:
            print(f"Error opening file: {error.message}")

    def on_export(self, *args):
        self.open_dialog = Gtk.FileDialog.new()
        self.open_dialog.set_title("Export a File")
        self.open_dialog.save(self, None, self.export_dialog_open_callback)
