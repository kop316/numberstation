#!/usr/bin/env python3
from urllib.parse import quote
import json
import sys

items = json.load(sys.stdin)
for item in items:
    if item["type"] != "TOTP":
        sys.stderr.write(f"Unknown code type {item['type']}\n")
    label = quote(item["label"])
    secret = item["secret"]
    url = f"otpauth://totp/{label}?secret={secret}"
    if "issuer" in item and item["issuer"]:
        issuer = quote(item["issuer"])
        url += f"&issuer={issuer}"
    print(url)
